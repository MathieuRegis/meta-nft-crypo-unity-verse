 using System.Collections;
using System.Threading.Tasks;
using Thirdweb;
using UnityEngine;
using UnityEngine.Networking;

public class Web3 : MonoBehaviour
{
    private ThirdwebSDK sdk;
    private string assetBundleUrl;

    async void Start()
    {
        sdk = new ThirdwebSDK("VOTRE_RESEAU"); // Remplacez VOTRE_RESEAU par le réseau désiré, par exemple : "optimism-goerli"
        await LoadNft();
        StartCoroutine(SpawnNft());
    }

    async Task<string> LoadNft()
    {
        var contract = sdk.GetContract("VOTRE_ADRESSE_CONTRAT"); // Remplacez VOTRE_ADRESSE_CONTRAT par l'adresse de votre contrat
        var nft = await contract.ERC721.Get("0");
        assetBundleUrl = nft.metadata.image;
        return assetBundleUrl;
    }

    IEnumerator SpawnNft()
    {
        string assetName = "Cube"; // Nom de l'objet dans l'AssetBundle

        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(assetBundleUrl);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Network error: " + www.error);
        }
        else
        {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            GameObject prefab = bundle.LoadAsset<GameObject>(assetName);
            GameObject instance = Instantiate(prefab, new Vector3(0, 3, 5), Quaternion.identity);

            Material material = instance.GetComponent<Renderer>().material;
            material.shader = Shader.Find("Standard");
        }
    }
}
