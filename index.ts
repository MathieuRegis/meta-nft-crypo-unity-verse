import { ThirdwebSDK, Network } from "thirdweb";
import * as dotenv from "dotenv";

dotenv.config();

const PRIVATE_KEY = process.env.PRIVATE_KEY || "";
const THIRDWEB_API_KEY = process.env.THIRDWEB_API_KEY || "";

if (!PRIVATE_KEY || !THIRDWEB_API_KEY) {
    console.error("Veuillez définir PRIVATE_KEY et THIRDWEB_API_KEY dans votre fichier .env");
    process.exit(1);
}

const sdk = new ThirdwebSDK({
    network: Network.MUMBAI,
    privateKey: PRIVATE_KEY,
    apiKey: THIRDWEB_API_KEY
});

const CONTRACT_ADDRESS = "ADRESSE_DU_CONTRAT";
const IPFS_URI = "ipfs://Qm...";

async function mintNFT() {
    try {
        const contract = sdk.getContract(CONTRACT_ADDRESS);
        const tokenId = await contract.mint({
            tokenURI: IPFS_URI
        });

        console.log(`NFT minté avec succès. Token ID : ${tokenId}`);
    } catch (error) {
        console.error(`Erreur lors de la création du NFT : ${error.message}`);
    }
}

mintNFT();
 
